#!/usr/bin/env python3
# -*- coding: utf-8 -*-

'''
Name:           station.py
Description:    manage internet radio stations
Usage:          ./station.py
Author:         Ralf Hersel - ralf.hersel@gmx.net
License:        GPL3
Version, Date:  (see constants)

Dependencies:
vlc (cvlc)                                                                      # media player (GUI and CLI)
pyradios   - pip3 install pyradios                                              # required to calculate mp3 duration
mpv                                                                             # for playback
'''

# === Todo =====================================================================

# Refactor record_station (Streamripper is outdated; check if any work at all.)


# === Libraries ================================================================

import subprocess
import time
import os
from os.path import expanduser
from pyradios import RadioBrowser                                               # https://www.radio-browser.info


# === Constants ================================================================

STATION_FILENAME = 'station.csv'
VERSION = '1.35 - 2023-06-25'
PLAYER = 'mpv'                                                                  # clcv or mpv
CONTROLS = '''Player controls:
    q       quit
    space   pause/continue
    right   fast forward
    left    fast backward'''
HELP = f"""
================================================================================
STATION Version {VERSION}
--------------------------------------------------------------------------------
s       = show list of stations and genres
[x]     = play station number x (requires VLC or Mplayer)
test    = play all stations for 3 seconds
d [x]   = delete station number x from list
si [s]  = search internet for station by name (no quotes) and add it
get     = get data for all stations from Radio-Browser
rs [x]  = record station number x (requires Streamripper)
rv [x]  = record station number x (requires VLC)
ht      = generate html file
rt      = generate xml file for RadioTray
rb      = generate xml file for Rhythmbox
ur      = generate xml for uRadio
m3u     = generate m3u playlist for MPD
q       = quit
================================================================================
"""

# === Functions ================================================================

def read_stations(filename):                                                    # read stations from csv, return list
    stations = []
    rec_lenght = 4
    f = open(filename)
    for row in f:
        rec = row.strip().split('|')
        if len(rec) == rec_lenght:
            stations.append(rec)                                                # append valid recs to station list
    f.close()
    return stations


def show(stations):                                                             # show list of stations and genres
    genres = {}
    count = 0
    
    print("\n========== Stations ===============================================")
    for row in stations:
        count += 1
        strcount = str(count)
        print(strcount.ljust(4, ' '), row[0].ljust(40, ' '), row[1].ljust(15, ' '), row[3])
        try: genres[row[0]] = genres[row[0]] + 1                                # count genre
        except KeyError: genres[row[0]] = 1                                     # first assignment
    print("===================================================================")


def del_station(stations, x):                                                   # delete station from list and write back
    name = stations[x-1][0]
    print('Deleted: ' + name)
    del stations[x-1]
    return stations
    

def write_stations(stations, filename):                                         # write station list to csv
    stations.sort()                                                             # sort by name
    f = open(filename, 'w')
    for s in stations:
        rec = '|'.join(s) + '\n'
        f.write(rec)
    f.close()


def write_html(stations):
    html = """
    <html>
    <table border="0" cellpadding="5">
    <tbody>
    <tr style="background-color: #00538a;">
    <td><strong><span style="color: #ffffff;">&nbsp;Name</span></strong></td>
    <td><strong><span style="color: #ffffff;">&nbsp;Genre</span></strong></td>
    <td><strong><span style="color: #ffffff;">&nbsp;Stream URL</span></strong></td>
    <td><strong><span style="color: #ffffff;">&nbsp;Quality</span></strong></td>
    </tr>
    """
    
    for station in stations:
        genre = station[0]
        name = station[1]
        url = station[2]
        quality = station[3]
        table = """
        <tr>
        <td>&nbsp;%s</td>
        <td>&nbsp;%s</td>
        <td>&nbsp;%s</td>
        <td>&nbsp;%s</td>
        </tr>
        """ % (genre, name, url, quality)
        html += table
        
    footer = """
    </tbody>
    </table>
    </html>
    """

    html += footer
    
    f = open('station.html', 'w')
    f.write(html)
    f.close()
    print("Wrote file 'station.html'. Open it in your webbrowser.")


def write_radiotray(stations):
    xml = """<bookmarks>\n<group name="root">\n"""
    
    last_genre = ''
    for station in stations:
        genre = station[0]
        name = station[1]
        url = station[2]
        if genre != last_genre:
            if last_genre == '':
                xml += """\t<group name="%s">\n""" % (genre)
            else:
                xml += """\t\t</group>\n\t<group name="%s">\n""" % (genre)
            
        xml += """\t\t<bookmark name="%s" url="%s"/>\n""" % (name, url)
        last_genre = genre
        
    xml += "\t\t</group>\n\t\t</group>\n</bookmarks>"
    
    f = open('bookmarks.xml', 'w')
    f.write(xml)
    f.close()
    print("Wrote file 'bookmarks.xml' for RadioTray. Copy it to /home/user/.local/share/radiotray/")


def write_rhythmbox(stations):
    xml = ''
    for station in stations:
        genre = station[0]
        name = station[1]
        url = station[2]
        xml += """
        <entry type="iradio">
            <title>%s</title>
            <genre>%s</genre>
            <artist></artist>
            <album></album>
            <location>%s</location>
            <date>0</date>
            <media-type>application/octet-stream</media-type>
        </entry>
        """ % (name, genre, url)
    
    f = open('rhythmbox.xml', 'w')
    f.write(xml)
    f.close()
    print("Wrote file 'rhythmbox.xml' for Rhythmbox radio entries. Merge it in /home/user/.local/share/rhythmbox/rhythmdb.xml")


def write_m3u(stations):                                                        # write m3u playlist for MPD and copy it to Raspi4
    m3u = '#EXTM3U\n'
    prefix = '#EXTINF:,'
    for station in stations:
        name = station[0]
        genre = station[1]
        quality = station[3]
        name_genre = prefix + name + ' (' + genre + ', ' + quality + ')\n'
        url = station[2] + '\n'
        m3u += name_genre + url
    
    f = open('Radio.m3u', 'w')
    f.write(m3u)
    f.close()
    print("Wrote file 'Radio.m3u' for MPD. Please copy it to the Playlist folder on NAS.")


def write_uradio(stations):
    xml = ''
    for station in stations:
        genre = station[0]
        name = station[1]
        url = station[2]
        xml += """
        == Daten uRadio ==
        <nowiki>
        <station>
            <name>%s</name>
            <description>%s</description>
            <image>http://uradio.foroubuntu.es/images/station_image.png</image>
            <audio>%s</audio>
        </station>
        </nowiki>       
        """ % (name, genre, url)
    
    f = open('uradio.xml', 'w')
    f.write(xml)
    f.close()
    print("Wrote file 'uradio.xml' for uRadio entries. Copy it to http://uradio.foroubuntu.es/index.php/Category:Radio_Stations")


def test_stations(stations):                                                    # play each station for 5 sec. and check if it is ok
    bad_stations = []
    waittime = 3
    
    for station in stations:
        name = station[0]
        genre = station[1]
        url = station[2]
        quality = station[3]
        print('---------------------------------------------------------')
        print('Playing: ', name, ' (', genre, quality, ')')
        print('---------------------------------------------------------')
        p = subprocess.Popen([PLAYER, url])                                     # play the stream with CVLC
        time.sleep(waittime)                                                    # wait a while
        p.terminate()                                                           # stop playing
        print('---------------------------------------------------------')
        if input('\nWas it ok? (y/n)\n\n') != 'y':
            bad_stations.append(station)
    
    print('--------------------------------------------------------')
    print('Bad stations')
    print('--------------------------------------------------------')
    for bad in bad_stations:
        print(bad)


def play_station(stations, x):                                                  # play station number n
    station = stations[x-1]
    genre = station[1]
    name = station[0]
    url = station[2]
    
    print('--------------------------------------------------------')
    print('Playing: ', name, genre)
    print(CONTROLS)
    print('--------------------------------------------------------')
    
    p = subprocess.Popen([PLAYER, url])                                         # play the song with MPV or CLVC
    p.wait()
    p.terminate()


def record_station(stations, x, command):                                       # record station x with Streamripper or CVLC
    station = stations[x-1]
    name = station[0]
    genre = station[1]
    url = station[2]
    
    home_folder = expanduser("~")
    
    if command == 'rs':
        method = 'streamripper'
        
    elif command == 'rv':
        method = PLAYER
    else:
        print('Error: unknown recording method:', command)
        exit(1)
    
    print('--------------------------------------------------------')
    print('Recording: ', name, genre, 'using', method)
    print('--------------------------------------------------------')
    
    #~ streamripper http://broadcast.infomaniak.ch/energyzuerich-high.mp3.m3u -d /home/title.mp3
    #~ cvlc -v http://www.iloveradio.de/ilove2dance.m3u --sout=file/mp3:/home/station_name.mp3
    
    if method == 'streamripper':
        p = subprocess.Popen([method, url, '-d', home_folder])
    elif method == PLAYER:
        p = subprocess.Popen([method, '-v', url, '--sout=file/mp3:' + home_folder + '/' + name + '.mp3'])
        
    input('\nPress any Key+Return or Ctrl+c to stop recording\n')
    p.terminate()                                                               # stop recording
    print("\nCheck your Home folder for the recordings.\n")


def get_radio_browser(stations):                                                # get all station data from Radio-Browser
    rb = RadioBrowser()
    for station in stations:
        station_name = station[0]
        response = rb.search(name=station_name, name_exact=True)                # get station data
        if len(response) > 0:
            data = response[0]                                                  # get first search result from list
            print('---------------------------------------------------')
            print('Name   :', data['name'])
            print('Tags   :', data['tags'])
            print('Bitrate:', data['bitrate'])
            print('Country:', data['country'])
            print('URL    :', data['url'])
            print('Stream :', data['url_resolved'])
            print()
        else:
            print('---------------------------------------------------')
            print(station_name, 'not found')


def search_internet(stations, name):                                            # Search Radio Browser DB
    rb = RadioBrowser()
    data = rb.search(name=name, name_exact=True)
    station_id = 0
    if len(data) > 0:
        search_dict = {}                                                        # create dict for search results
        print('---------------------------------------------------')
        for station in data:                                                    # show found stations
            station_id += 1
            print('Id     :', station_id)
            print('Name   :', station['name'])
            print('Tags   :', station['tags'])
            print('Bitrate:', station['bitrate'])
            print('Country:', station['country'])
            print('URL    :', station['url'])
            print('Stream :', station['url_resolved'])
            print('---------------------------------------------------')
            search_dict[station_id] = station                                   # add found station to dict
        
        result = input('Do you want to add a station? (y/n): ')
        if result == 'y':
            input_id = int(input('Please enter the station id: '))              # get station id and convert it to int
            try:
                station = search_dict[input_id]
                genre = input('Please enter a genre: ')
                rec = [station['name'], genre, station['url_resolved'], str(station['bitrate'])]
                stations.append(rec)
                changed = True
            except KeyError:
                print('This Id does not exist')
                changed = False
        else:
            changed = False
    else:
        print('No stations matches your search')
        changed = False

    return stations, changed


#~ === Main ====================================================================

def main(args):
    stations = read_stations(STATION_FILENAME)                                  # get radio stations from csv file
    # command, param = commandline()                                              # get commandline arguments
    command = ''                                                                # initialize, to not leave the loop
    while command != 'q':                                                       # quit
        print(HELP)
        command = input('STATION > ')
        cmd_list = command.split(' ')
        command = cmd_list[0]
        if len(cmd_list) > 1:
            param = cmd_list[1]
        if command == '':
            message = 'Enter a command'
        if command == False:
            return 0
        if command == "s":                                                      # show list of stations
            show(stations)
        elif command == "ht":                                                   # generate html file
            write_html(stations)
        elif command == 'rt':                                                   # generate xml file for RadioTray
            write_radiotray(stations)
        elif command == 'rb':                                                   # generate xml file for Rhythmbox
            write_rhythmbox(stations)
        elif command == 'ur':                                                   # generate xml file for uRadio
            write_uradio(stations)
        elif command == 'm3u':                                                  # generate m3u playlist for MPD
            write_m3u(stations)
        elif command == 'test':                                                 # play each station for 5 sec.
            test_stations(stations)
        elif command == 'get':                                                  # get data for all stations from Radio-Browser
            get_radio_browser(stations)
        elif command.isdigit():                                                 # play station number x
            x = int(command)
            if x > len(stations):
                print('This station number does not exist')
            else:
                play_station(stations, x)
        elif command in ['rs','rv', 'd']:
            x = int(param)
            if x > len(stations):
                print('This station number does not exist')
            elif command == 'd':
                stations = del_station(stations, x)
                write_stations(stations, STATION_FILENAME)
            elif command == 'rs':
                record_station(stations, x, command)
        elif command == 'si':
            name = str(param)
            if name == '':
                print('Station name is missing')
            else:
                stations, changed = search_internet(stations, name)
                if changed:
                    write_stations(stations, STATION_FILENAME)
                    print('New station added to CSV')
                    stations = read_stations(STATION_FILENAME)                  # update radio stations from csv file
        elif command == 'q':
            print('Thank you for using STATION')
            finish = True
        else:
            print('Error: invalid input')
            finish = True
    return 0


if __name__ == '__main__':
    import sys
    sys.exit(main(sys.argv))
