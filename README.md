# ![Logo](img/station.png) Welcome to Station

Station is a simple Python CLI application that helps organizing Internet-Radio-Stations.

![YouPlay Screenshot CLI](img/station_menu.png)

With **Station** you can do a lot:

* show your stations
* play a station
* run a 5 seconds playback test for all your stations to check if they are OK
* delete stations from your library
* search for IRS by name at https://www.radio-browser.info
* get information for all your stations from https://www.radio-browser.info
* record a stream from a station
* record single MP3 files from a station
* generate html with all your stations
* generate xml for RadioTray and uRadio
* generate a m3u-playlist for your prefered audio player

If you think that is cool, go on with the installation.

## Installation

Since Station is a simple Python file, the installation has no bells and whistles. Simply go to the folder under which you would like to install the application and run these commands in a terminal:

```bash
git clone https://codeberg.org/ralfhersel/station.git
cd station
```

Station has a few dependencies. Please install these packages with the package manager of your GNU/Linux distribution:

> `mpv`, `pyradios`, `vlc`

If anything is missing, Python will tell you when you start Station for the first time like this:

```bash
cd station
python station.py
or
python3 station.py
```

Station is a CLI application. When you run it in a terminal, it will show an interactive command menu. Here is how to start it:

```bash
python station.py
```

All station data is stored in a CSV-File: `station.csv`. The installation comes with a pre-filled list of stations.

## Release Notes

###### Version 1.35 - 2023-06-25

* Interactive command menu

###### Version 1.34 - 2022-02-18

* first push

###### Earlier versions

* not documented - sorry

## License

Author: Ralf Hersel

License: GPL3

## Contact

ralf.hersel@gmx.net
